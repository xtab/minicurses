#include <stdio.h>
#include <minicurses.h>


char* codes[] = {"ELBA", "CIME", "DEBA", "JILL", "ELBA", "CIME"};
char* dest[] = {
	"St-Martin Etampes",
	"Vers. Chantiers",
	"Dourdan",
	"Juvisy",
	"St-Martin Etampes",
	"Vers. Chantiers"
};
char* heures[] = {
	"17:54", "17:58", "18:07", "18:13", "18:22", "18:27"
};
char* quais[] = {"B", "B", "B", "B", "B", "B"};

int main(int argc, char** argv)
{
	int i = 0;
	WINDOW* trains;

	initscr();
	start_color();
	curs_set(0);

	init_pair(1, COLOR_WHITE, COLOR_RED);
	init_pair(2, COLOR_WHITE, COLOR_BLACK);
	init_pair(3, COLOR_CYAN, COLOR_BLUE);
	init_pair(4, COLOR_MAGENTA, COLOR_BLUE);

	trains = newwin(6, 40, 7, 0);

	mvaddstr(1, 0, "De : ");
	mvaddstr(3, 0, "A :  ");
	attron(COLOR_PAIR(1) | A_DOUBLEHEIGHT);
	mvprintw(1, 5, " %-19s\n", "Invalides");
	mvprintw(3, 5, " %-19s\n", "Juvisy");
	attroff(COLOR_PAIR(1) | A_DOUBLEHEIGHT);

	attron(A_REVERSE | COLOR_PAIR(2));
	mvprintw(6, 0, " %-4s %-17s %-10s %-4s ", 
		"Nom", "Destination", "Heure", "Quai");
	attroff(A_REVERSE | COLOR_PAIR(2));

	for (i = 0; i < 6; i++) {
		if (i % 2 == 1) {
			wattron(trains, COLOR_PAIR(3));
			mvwaddstr(trains, i, 0, "                                       ");
		}
		mvwaddstr(trains, i, 1, codes[i]);
		mvwaddstr(trains, i, 6, dest[i]);
		mvwaddstr(trains, i, 24, heures[i]);
		mvwaddstr(trains, i, 35, quais[i]);
		if (i % 2 == 1) {
			wattroff(trains, COLOR_PAIR(3));
		}
	}

	mvaddstr(20, 0, "Info RATP Ligne 13 Trafic normal");
	mvaddstr(21, 0, "La ligne C vous souhaite une bonne");
	mvaddstr(22, 0, "journ�e");

	mvaddstr(23, 0, "Autre demande : ...... tapez ");
	attron(COLOR_PAIR(4));
	addstr(" Retour ");
	attroff(COLOR_PAIR(4));

	wnoutrefresh(trains);
	doupdate();

	curs_set(1);

	delwin(trains);

	return 0;
}
