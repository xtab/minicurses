#include <minicurses.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
	PIXMAP *pixmap, *pixmap2, *pixmap3, *rose;

	initscr();

	start_graphics(argv[0]);
	init_image(&pixmap,  "gfx/3615x0r-bw.pbm");
	init_image(&pixmap2, "gfx/3615x0r-gs.pgm");
	init_image(&pixmap3, "gfx/3615x0r-c.ppm");

	mvaddimg(0, 0, pixmap);
	mvaddimg(7, 0, pixmap2);
	mvaddimg(14, 0, pixmap3);
	refresh();

	getc(stdin);

	destroy_image(pixmap);
	destroy_image(pixmap2);
	destroy_image(pixmap3);

	init_image(&rose, "gfx/rose.pgm");

	clear();
	mvaddimg(0, 2, rose);
	refresh();
	getc(stdin);

	endwin();
	return 0;
}
