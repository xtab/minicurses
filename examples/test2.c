#include <minicurses.h>

int main(int argc, char** argv)
{
	initscr();
	start_color();

	init_pair(1, COLOR_BLUE, COLOR_BLACK);
	init_pair(2, COLOR_RED,  COLOR_BLACK);
	init_pair(3, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(4, COLOR_GREEN, COLOR_BLACK);
	init_pair(5, COLOR_CYAN, COLOR_BLACK);
	init_pair(6, COLOR_YELLOW, COLOR_BLACK);
	init_pair(7, COLOR_WHITE, COLOR_BLACK);

	attron(A_DOUBLESIZE); 	addstr("gros "); 	attroff(A_DOUBLESIZE);
	attron(A_DOUBLEHEIGHT); addstr("grand "); 	attroff(A_DOUBLEHEIGHT);
	attron(A_DOUBLEWIDTH); 	addstr("large "); 	attroff(A_DOUBLEWIDTH);
	attron(A_REVERSE); 	addstr(" inverse "); 	attroff(A_REVERSE);

	move(3, 0);
	attron(COLOR_PAIR(1)); addstr(" bleu  \n");	attroff(COLOR_PAIR(1));
	attron(COLOR_PAIR(2)); addstr(" rouge \n");	attroff(COLOR_PAIR(2));
	attron(COLOR_PAIR(3)); addstr(" magenta\n"); 	attroff(COLOR_PAIR(3));
	attron(COLOR_PAIR(4)); addstr(" vert  \n");	attroff(COLOR_PAIR(4));
	attron(COLOR_PAIR(5)); addstr(" cyan \n");	attroff(COLOR_PAIR(5));
	attron(COLOR_PAIR(6)); addstr(" jaune \n");	attroff(COLOR_PAIR(6));
	attron(COLOR_PAIR(7)); addstr(" blanc \n");	attroff(COLOR_PAIR(7));

	mvaddstr(11, 0, "Le chat de Schr\366dinger ");
	attron(A_BLINK); addstr("n'"); attroff(A_BLINK);
	addstr("est ");
	attron(A_BLINK); addstr("pas"); attroff(A_BLINK);
	addstr(" mort.");

	refresh();

	return 0;
}
