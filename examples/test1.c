#include <minicurses.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>



int main(int argc, char** argv) 
{
	char c;

	initscr();
	if (start_color() == ERR)
		return 2;

	if (init_pair(1, COLOR_WHITE, COLOR_RED) == ERR)
		return 1;

	attron(A_DOUBLESIZE);
	mvprintw(1, 0, "%d %s", 3615, "X0R.FR");
	attroff(A_DOUBLESIZE);

	attron(A_DOUBLEHEIGHT | COLOR_PAIR(1));
	mvaddstr(4, 0, " Menu principal \n");
	attroff(A_DOUBLEHEIGHT | COLOR_PAIR(1));

	addstr(" 1 - Blog\n");
	addstr(" 2 - Photos\n");
	addstr(" 3 - Contact\n");
	addstr(" 4 - BitBucket\n");

	attron(A_DOUBLEHEIGHT | COLOR_PAIR(1));
	addstr(" Derniers articles \n");
	attroff(A_DOUBLEHEIGHT | COLOR_PAIR(1));


	refresh();
	c = getc(stdin);

	endwin();
	return 0;
}


