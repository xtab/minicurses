#include <minicurses.h>
#include <stdio.h>
#include <unistd.h>



void fill_with(char c)
{
	int i;

	fprintf(stderr, "filling with %c\n", c);
	for (i = 0; i < COLS * LINES; i++)
		addch(c);
}

void call_refresh()
{
	fprintf(stderr, "refreshing\n");
	refresh();
}


int main(int argc, char** argv)
{
	initscr();

	/* test 1 : refresh sans rien */
	fill_with('A');
	call_refresh();
	sleep(2);
	fill_with('B');
	call_refresh();
	sleep(2);
	erase();
	fill_with('C');
	call_refresh();
	sleep(2);
	clear();
	fill_with('D');
	call_refresh();
	sleep(3);
	endwin();
	return 0;
}
