#include <minicurses.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
	initscr();

	attron(A_DOUBLEHEIGHT);
	addstr("Test jeu de caract�res");
	attroff(A_DOUBLEHEIGHT);

	move(3, 0);

	addstr("Portez ce vieux whisky au juge blond qui");
	addstr("fume sur son �le int�rieure, � c�t� de\n");
	addstr("l'alc�ve ovo�de, o� les b�ches se con-\n");
	addstr("sument dans l'�tre, ce qui lui permet de");
	addstr("penser � la caenog�n�se de l'�tre dont\n");
	addstr("il est question dans la cause ambigu�,\n");
	addstr("dans un capharna�m qui, pense-t-il, di-\n");
	addstr("minue �a et l� la qualit� de son �uvre.\n\n");
	refresh();

	mvaddstr(11, 0, "� ESZETT");
	mvaddstr(13, 0, "� DEGRE");
	mvaddstr(14, 0, "� PLUS-MOINS");
	refresh();

	fgetc(stdin);
	return 0;
}
