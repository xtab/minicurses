/*
 * Copyright (c) 2012-2013, x0r
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */

#include <stdlib.h>
#include <assert.h>

#include "include/minicurses.h"
#include "window.h"

struct WINDOW {
	int	 cur_y, cur_x;	/* cursor position */
	uint32_t cur_attr;	/* cursor attributes */
	int	 beg_y, beg_x;	/* physical location of (0, 0) */
	int	 lines, cols;	/* height and width of window */
	mchar*	 data;		/* window data (lines * cols array) */
	uint8_t	 outopts;	/* holds stuff like clearok */
};

WINDOW* stdscr;



/* PRIVATE FUNCTIONS */

int __cursor_getpos(const WINDOW* win)
{
	return win->cur_y * win->cols + win->cur_x;
}


void __cursor_bounds_check(WINDOW* win)
{
	if (win == NULL) return;

	if (win->cur_x < 0 || win->cur_x >= win->cols)
	{
		win->cur_y += win->cur_x / win->cols;
		win->cur_x %= win->cols;
	}
	win->cur_y %= win->lines;

	assert(win->cur_y >= 0 && win->cur_y < win->lines);
	assert(win->cur_x >= 0 && win->cur_x < win->cols);
}


int __cursor_forward(WINDOW* win)
{
	win->cur_x++;
	if (win->cur_attr & A_DOUBLEWIDTH)
		win->cur_x++;
	__cursor_bounds_check(win);
	return __cursor_getpos(win);
}

int __cursor_back(WINDOW* win)
{
	win->cur_x--;
	if (win->cur_attr & A_DOUBLEWIDTH)
		win->cur_x--;
	__cursor_bounds_check(win);
	return __cursor_getpos(win);
}

int __cursor_up(WINDOW* win)
{
	win->cur_y--;
	if (win->cur_attr & A_DOUBLEHEIGHT)
		win->cur_y--;
	__cursor_bounds_check(win);
	return __cursor_getpos(win);
}

int __cursor_down(WINDOW* win)
{
	win->cur_y++;
	if (win->cur_attr & A_DOUBLEHEIGHT)
		win->cur_y++;
	__cursor_bounds_check(win);
	return __cursor_getpos(win);
}

int __cursor_move(WINDOW* win, const int y, const int x)
{
	win->cur_y = y;
	win->cur_x = x;
	__cursor_bounds_check(win);
	return __cursor_getpos(win);
}



/* EXPORTED FUNCTIONS */

WINDOW* newwin(int nlines, int ncols, int begin_y, int begin_x)
{
	WINDOW* win;
	int i;

	/* bounds check begin coordinates */
	if (begin_y >= LINES) return NULL;
	if (begin_x >= COLS) return NULL;

	/* set defaults for nlines and ncols */
	nlines 	= (nlines == 0) ? LINES - nlines  : nlines;
	ncols	= (ncols == 0)  ? COLS - ncols : ncols;

	win = malloc(sizeof(struct WINDOW));

	if (win == NULL)
		return win;

	/* set structure attributes */
	win->cur_y = 0;
	win->cur_x = 0;

	win->beg_y = begin_y;
	win->beg_x = begin_x;
	win->lines = nlines;
	win->cols  = ncols;

	win->cur_attr = 0;

	/* initialize window data with empty chars */
	win->data  = malloc(nlines * ncols * sizeof(mchar));
	if (win->data == NULL) {
		free(win);
		return NULL;
	}

	for (i = 0; i < nlines * ncols; i++) {
		win->data[i] = ' ';
	}

	return win;
}



int delwin(WINDOW* win)
{
	if (win == NULL)
		return ERR;

	if (win->data != NULL)
		free(win->data);

	free(win);
	return OK;
}




int werase(WINDOW* win)
{
	int i;
	CHECK_VALID_WINDOW(win);

	for (i = 0; i < win->lines * win->cols; i++) {
		win->data[i] = ' ';
	}
	return OK;
}




int wmove(WINDOW* win, int y, int x)
{
	CHECK_VALID_WINDOW(win);
	if (y < 0 || y >= win->lines || x < 0 || x >= win->cols)
		return ERR;

	__cursor_move(win, y, x);
	return OK;
}




int wnoutrefresh(WINDOW* win)
{
	int bx, by;
	int pos;

	CHECK_VALID_WINDOW(win);
	if (win == stdscr)
		return OK;

	bx = win->beg_x;
	by = win->beg_y;

	for (pos = 0; pos < win->lines * win->cols; pos++)
	{
		int y = pos / win->cols;
		int x = pos % win->cols;

		stdscr->data[bx + x + COLS * (by + y)] = win->data[pos];
	}

	return OK;
}



int waddch(WINDOW* win, const chtype c)
{
	int pos;

	CHECK_VALID_WINDOW(win);

	pos = win->cur_y * win->cols + win->cur_x;
	switch (c)
	{
		case 0: /* NUL */
			break;
		case 8: /* BS  */
			win->data[pos] = ATTR_MASK;
			win->data[pos] |= ' ';
			pos = __cursor_back(win);
			break;
		case '\n':
			if (stdscr->outopts & OUTOPT_NONL)
				pos = __cursor_move(win,
						win->cur_y + 1, win->cur_x);
			else
				pos = __cursor_move(win, win->cur_y + 1, 0);
			break;
		case '\r':
			pos = __cursor_move(win, win->cur_y, 0);
			break;
		default:
			/* if control character, print its ^? form */
			if (c >= 0 && c < 32) {
				waddch(win, '^');
				waddch(win, c + 64);
			}
			else {
				win->data[pos] = win->cur_attr | c;
				__cursor_forward(win);
			}
	}

	return OK;
}



int waddstr(WINDOW* win, const char* s)
{
	CHECK_VALID_WINDOW(win);

	if (s == NULL)
		return ERR;

	while (*s)
		waddch(win, (chtype)(unsigned char)(*s++));

	return OK;
}





int wattron(WINDOW* win, uint32_t attr)
{
	CHECK_VALID_WINDOW(win);

	/* This is contrary to most people's assumptions */
	if (attr & A_DOUBLEHEIGHT)
		__cursor_down(win);

	if (attr & COLOR_PAIR_MASK)
		wattrset(win, win->cur_attr & (ATTR_MASK ^ COLOR_PAIR_MASK));

	if (attr & A_DOUBLESIZE)
		wattrset(win, win->cur_attr & (ATTR_MASK ^ A_DOUBLESIZE));

	wattrset(win, win->cur_attr | attr);

	return OK;
}




int wattroff(WINDOW* win, uint32_t attr)
{
	CHECK_VALID_WINDOW(win);

	if (attr & COLOR_PAIR_MASK) {
		wattrset(win, win->cur_attr & (ATTR_MASK ^ COLOR_PAIR_MASK));
		attr &= (ATTR_MASK ^ COLOR_PAIR_MASK);
	}
	if (attr & A_DOUBLESIZE) {
		wattrset(win, win->cur_attr & (ATTR_MASK ^ A_DOUBLESIZE));
		attr &= (ATTR_MASK ^ A_DOUBLESIZE);
	}
	
	wattrset(win, win->cur_attr & (ATTR_MASK ^ attr));

	return OK;
}



int wattrset(WINDOW* win, uint32_t attr)
{
	CHECK_VALID_WINDOW(win);

	win->cur_attr = attr;

	return OK;
}





/* PUBLIC FUNCTIONS */

uint8_t get_outopts(WINDOW* win)
{
	if (win == NULL)
		return 0;
	
	return win->outopts;
}

int set_outopt(WINDOW* win, uint8_t outopts, int bf)
{
	CHECK_VALID_WINDOW(win);

	if (bf)
		win->outopts |= outopts;
	else
		win->outopts &= (OUTOPT_MASK ^ outopts);
	
	return OK;
}

int cursor_getattr(WINDOW* win)
{
	if (win != NULL)
		return win->cur_attr;
	else
		return 0;
}


int cursor_getx(WINDOW* win)
{
	if (win != NULL)
		return win->cur_x;
	else
		return -1;
}


int cursor_gety(WINDOW* win)
{
	if (win != NULL)
		return win->cur_y;
	else
		return -1;
}

