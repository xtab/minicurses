/*
 * Copyright (c) 2012-2013, x0r
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */


#include "config.h"
#include "include/minicurses.h"

#include "image.h"
#include "screen.h"
#include <stdlib.h>
#include "window.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifdef HAVE_NETPBM_PAM_H
#include <netpbm/pam.h>
#endif

#ifdef HAVE_PAM_H
#include <pam.h>
#endif

/* One macro introduced in netpbm 10.23 might actually not exist in
 * the library versions installed by Debian (guys, seriously...) */
#ifndef PAM_STRUCT_SIZE
#define PAM_MEMBER_OFFSET(mbrname) \
  ((size_t)(unsigned long)(char*)&((struct pam *)0)->mbrname)
#define PAM_MEMBER_SIZE(mbrname) \
  sizeof(((struct pam *)0)->mbrname)
#define PAM_STRUCT_SIZE(mbrname) \
  (PAM_MEMBER_OFFSET(mbrname) + PAM_MEMBER_SIZE(mbrname))
#endif


#define GET_PIXEL(pixmap, x, y) \
	(((x) >= (pixmap)->columns || (y) >= (pixmap)->rows) \
	 ? 0 \
	 : (pixmap)->data[(y)][(x)])

static int __pixmap_init_done = 0;


void pixmap_start(char* progname)
{
	if (! __pixmap_init_done) 
	{
		pm_init(progname, 0);
		__pixmap_init_done++;
	}
}



PIXMAP* pixmap_create(int rows, int columns)
{
	PIXMAP* ret;
	int i;

	if (rows <= 0 || columns <= 0)
		return NULL;

	ret = (PIXMAP*) malloc(sizeof(struct vtx_pixmap));
	if (ret == NULL)
		return NULL;

	ret->rows 	= rows;
	ret->columns 	= columns;
	ret->data 	= malloc(rows * sizeof(char*));
	
	if (ret->data == NULL)
	{
		free(ret);
		return NULL;
	}

	for (i = 0; i < rows; i++)
	{
		ret->data[i] = malloc(columns * sizeof(char));
		if (ret->data[i] == NULL)
		{
			for (i--; i >= 0; i--)
				free(ret->data[i]);
			free(ret);
			return NULL;
		}
	}

	return ret;
}


void pixmap_destroy(PIXMAP* pixmap)
{
	int i;

	if (pixmap == NULL)
		return;

	for (i = 0; i < pixmap->rows; i++)
		free(pixmap->data[i]);
	free(pixmap);
}




int __vdt_level(const tuple t, const char* t_type, const int maxval) {
	int ret = 0;

	int level, int_y;

	if (strcmp(t_type, PAM_PBM_TUPLETYPE) == 0) 
		if (t[0]) 
			level = 255;
		else
			level = 0;
	else if (strcmp(t_type, PAM_PGM_TUPLETYPE) == 0) 
		level = (t[0] * 250) / maxval;
	else
		level = 0.2126 * t[0] + 0.7152 * t[1] + 0.0722 * t[2];

	int_y = level;

	if (int_y >= 80 && int_y < 113)
		ret = 1;
	else if (int_y >= 114 && int_y < 139)
		ret = 2;
	else if (int_y >= 140 && int_y < 165)
		ret = 3;
	else if (int_y >= 166 && int_y < 191)
		ret = 4;
	else if (int_y >= 192 && int_y < 207)
		ret = 5;
	else if (int_y >= 208 && int_y < 233)
		ret = 6;
	else if (int_y >= 234)
		ret = 7;

	return ret;
}





PIXMAP* load_from_pnm(char* file)
{
	FILE* in;
	struct pam inpam;
	tuple* tuplerow;
	PIXMAP* pixmap;
	unsigned int y;

	int vdt_y;

	in = fopen(file, "r");
	if (in == NULL)
		return NULL;

	pnm_readpaminit(in, &inpam, PAM_STRUCT_SIZE(tuple_type));

	tuplerow = pnm_allocpamrow(&inpam);

	pixmap = pixmap_create(inpam.height, inpam.width);
	if (pixmap == NULL)
		return NULL;

	for (y = 0; y < inpam.height; y++)
	{
		unsigned int x;
		pnm_readpamrow(&inpam, tuplerow);
		for (x = 0; x < inpam.width; x++)
		{
			vdt_y = __vdt_level(tuplerow[x], 
					inpam.tuple_type, 
					inpam.maxval);
			pixmap->data[y][x] = vdt_y;
		}
	}

	pnm_freepamrow(tuplerow);

	return pixmap;
}




int waddimg(WINDOW* win, PIXMAP* pixmap)
{
	int x, y, i;
	int histogram[8];	/* # of pixels being colored some way */
	int block[6];		/* temp storage for a 6 px block */
	int maxcolor = 0; 	/* most frequent color */
	int maxcolor2 = 0;	/* second most frequent color */
	char graphic = 0;	/* graphic char used for output */
	uint16_t old_attr;
	int orig_x;

	if (win == NULL || pixmap == NULL)
		return ERR;

	old_attr = cursor_getattr(win);
	orig_x = cursor_getx(win);	/* original x position */


	for (y = 0; y < pixmap->rows; y += 3)
	{
		for (x = 0; x < pixmap->columns; x += 2)
		{
			/* reset values */
			for (i = 0; i < 8; i++)
				histogram[i] = 0;
			maxcolor  = 0;
			maxcolor2 = 0;

			/* copy pixel values */
			for (i = 0; i < 6; i++)
			{
				int dx = i % 2;
				int dy = i / 2;

				int px = GET_PIXEL(pixmap, x + dx, y + dy);
				assert(px >= 0);
				assert(px <= 7);

				block[i] = px;
				histogram[px]++;
			}

			/* determine most frequent colors */
			for (i = 0; i < 8; i++)
			{
				if (histogram[i] >= histogram[maxcolor])
				{
					maxcolor = i;
				}
			}

			if (maxcolor2 == maxcolor)
				maxcolor2 = 7;

			for (i = 0; i < 8; i++)
			{
				if (i == maxcolor)
					continue;
				if (histogram[i] > histogram[maxcolor2])
					maxcolor2 = i;
			}

			/* by convention, let maxcolor be the brightest
			 * of the two */
			if (maxcolor2 > maxcolor)
			{
				int temp = maxcolor2;
				maxcolor2 = maxcolor;
				maxcolor = temp;
			}

			/* generate graphic char */
			graphic = 0;
			for (i = 0; i < 6; i++)
			{
				if (abs(block[i] - maxcolor) 
			          < abs(block[i] - maxcolor2))
				{
					graphic |= (1 << i); /* foreground */
				}
				else
					block[i] = maxcolor2; /* background */
			}

			/* flip bits 5 and 6 */
			graphic = (((graphic & 0x20) << 1) | (graphic & 0x1f)) + 0x20;


			/* set color and output character */
			/* note that we filter the case when we obtain 0x7f (i.e. all
			 * pixels set), which doesn't work properly.  We exchange fg and bg
			 * colors instead and print a space.
			 */
			if (graphic != 0x7f)
			{
				wattrset(win, COLOR_PAIR((maxcolor2 << 3) | maxcolor) | A_GRAPHIC);
				addch(graphic);
			}
			else
			{
				wattrset(win, COLOR_PAIR((maxcolor << 3) | maxcolor2) | A_GRAPHIC);
				addch(' ');
			}

		}

		wmove(win, cursor_gety(win) + 1, orig_x);
	}

	wattrset(win, old_attr);
	return OK;
}

