/*
 * Copyright (c) 2012-2013, x0r
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */


/* for fileno(2) */
#define _POSIX_SOURCE 1

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <error.h>

#include "screen.h"
#include "videotex.h"
#include "window.h"


#define CHAR_WIDTH(mc) \
	(((mc) & A_DOUBLEWIDTH) ? 2 : 1)

#define RESET_ZONE_ATTRS(fl) ((fl) & (ATTR_MASK ^ COLOR_PAIR_MASK))
#define RESET_CHAR_ATTRS(fl) ((fl) & COLOR_PAIR_MASK)

#define MC_EQUALS(mc1, mc2) ((mc1) == (mc2))

#define ABS_MODULUS(x, y) (((x) + (y)) % (y))


struct WINDOW {
	int	 cur_y, cur_x;	/* cursor position */
	uint16_t cur_attr;	/* cursor attributes */
	int	 beg_y, beg_x;	/* physical location of (0, 0) */
	int	 lines, cols;	/* height and width of window */
	mchar*	 data;		/* window data (lines * cols array) */
	uint8_t	 outopts;	/* holds stuff like clearok */
};


SCREEN* __cur_screen;

static int __initscr_done = 0;



/* PUBLIC FUNCTIONS */


SCREEN* newterm(char* type, FILE* outfd, FILE* infd)
{
	SCREEN* scr = malloc(sizeof (struct screen));

	if (scr == NULL)
		return NULL;
	
	scr->cur_win 	  = newwin(0, 0, 0, 0);
	scr->new_win 	  = newwin(0, 0, 0, 0);
	scr->cur_statline = newwin(1, 0, 0, 0);
	scr->new_statline = newwin(1, 0, 0, 0);

	if (scr->cur_win == NULL
		|| scr->new_win == NULL
		|| scr->cur_statline == NULL
		|| scr->new_statline == NULL) {
		delscreen(scr);
		return NULL;
	}

	scr->outfd        = outfd;
	scr->infd	  = fileno(infd);

	return scr;
}




SCREEN* set_term(SCREEN* new)
{
	SCREEN* old = __cur_screen;
	__cur_screen = new;
	stdscr = __cur_screen->new_win;
	return old;
}




void delscreen(SCREEN* scr)
{
	if (scr == NULL)
		return;

	if (scr->cur_win != NULL)
		free(scr->cur_win);
	if (scr->new_win != NULL)
		free(scr->new_win);
	if (scr->cur_statline != NULL);
		free(scr->cur_statline);
	if (scr->new_statline != NULL);
		free(scr->new_statline);
	
	free(scr);
}




int doupdate() 
{
	int 		pos = 0;

	static mchar 	last_char 	= -1;
	static int 	last_char_count = 0;
	static int	last_char_pos 	= -1;

	/* We take care here not to use COLOR_PAIRs because we might not have
	 * initialized colors.  It's the programmer's responsibility not to
	 * mess things up.
	 */
	static uint32_t last_flags	= 0;

	FILE* 		outfd   = __cur_screen->outfd;
	WINDOW*		cur_win = __cur_screen->cur_win;
	

	/* check if clearok() has been called; if so, reset it */
	if (get_outopts(stdscr) & OUTOPT_CLEAROK)
	{
		minitel_cls(outfd);
		werase(cur_win);
		clearok(stdscr, 0);
		last_flags = 0;
	}

	/* redraw */
	for (pos = 0; pos < COLS * LINES; pos += CHAR_WIDTH(stdscr->data[pos]))
	{
		/* are the saved and to-be-painted mchars different? */
		if (! MC_EQUALS(cur_win->data[pos], stdscr->data[pos]))
		{
			uint32_t new_flags = stdscr->data[pos] & ATTR_MASK;

			/* offset char too far away? move to it */
			if (ABS_MODULUS(pos - last_char_pos, COLS * LINES) >= 3)
			{
				minitel_cup(outfd, 
					pos / COLS, pos % COLS);
				last_flags = RESET_CHAR_ATTRS(last_flags);
			}
			/* otherwise copy existing chars (if last_char_pos is initialized) */
			else if (last_char_pos != -1)
			{
				for (last_char_pos 
					+= CHAR_WIDTH(
						cur_win->data[last_char_pos]); 

					ABS_MODULUS(
						pos - last_char_pos, 
						COLS * LINES) != 0; 

					last_char_pos 
					+= CHAR_WIDTH(
						cur_win->data[last_char_pos])) 
				{
					last_char_pos %= COLS * LINES;

					minitel_chattr(outfd,
						cur_win->data[pos] & ATTR_MASK, 
							last_flags);
					minitel_char(outfd,
						cur_win->data[last_char_pos] & A_CHARTEXT);
					last_flags = cur_win->data[pos] & ATTR_MASK;
				}
			}

			/* switch to new flags from last flags */
			minitel_chattr(outfd, new_flags, last_flags);

			/* reprint char */
			minitel_char(outfd, stdscr->data[pos] & A_CHARTEXT);

			cur_win->data[pos] = stdscr->data[pos];
			last_flags = new_flags;

			/* check for repeated chars */
			last_char = stdscr->data[pos];
			last_char_count = 0;

			for (pos += CHAR_WIDTH(stdscr->data[pos]); 
					   pos < COLS * LINES 
					&& MC_EQUALS(stdscr->data[pos], last_char);
				pos += CHAR_WIDTH(stdscr->data[pos]))
			{
				cur_win->data[pos] = last_char;
				last_char_count++;
			}

			/* repeat char if needed */
			pos--;
			if (last_char_count > 2) 
				minitel_rep(outfd, last_char_count);
			else
				for (; last_char_count > 0; last_char_count--)
					minitel_char(outfd, last_char & A_CHARTEXT);

			/* save last updated character position */
			last_char_pos = pos;

			/* save stdscr position */
			cur_win->data[pos] = stdscr->data[pos];
		}
		/* catch zone attribute delimiters */
		else 
		{
			mchar mc      = cur_win->data[pos];
			mchar prev_mc = cur_win->data[pos - 1];

			if (pos % COLS == COLS - 1)
				last_flags = RESET_ZONE_ATTRS(last_flags);
			if (pos > 0 
				&& (mc & A_CHARTEXT) == 32 
				&& COLOR_PAIR_OF(mc) != COLOR_PAIR_OF(prev_mc))
			{
				last_flags = RESET_ZONE_ATTRS(last_flags);
			}
			if (mc & A_GRAPHIC)
				last_flags = RESET_ZONE_ATTRS(last_flags);
		}
	}	

	minitel_flush(outfd);
	return OK;

}







void stdscr_init() 
{
	if (__initscr_done)
		return;

	__cur_screen = newterm("minitel1b", stdout, stdin);
	if (__cur_screen == NULL)
		error(1, 0, "Could not initialize screen");

	stdscr = __cur_screen->new_win;

	minitel_cls(__cur_screen->outfd);
	minitel_cup(__cur_screen->outfd, 0, 0); /* redundant? */

	__initscr_done = 1;
}



void stdscr_destroy()
{
	if (!__initscr_done)
		return;

	minitel_cls(__cur_screen->outfd);
	delscreen(__cur_screen);

	__initscr_done = 0;
}






