/*
 * Copyright (c) 2012-2013, x0r
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */


#include "include/minicurses.h"
#include "window.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>



/* shamefully stolen from printf(3) man page */
static char* __vmake_message(const char* fmt, va_list ap) {
	int n, size = 100;
	char* p;

	if ((p = malloc(size * sizeof(char))) == NULL)
		return NULL;
	while(1) {
		n = vsnprintf(p, size, fmt, ap);
		if (n > -1 && n < size)
			return p;
		if (n > -1)
			size = n + 1;
		else
			size *= 2;
		if ((p = realloc(p, size)) == NULL)
			return NULL;
	}
}

int vwprintw(WINDOW* win, const char* fmt, va_list ap) {
	char* message;

	message = __vmake_message(fmt, ap);
	if (message == NULL)
		return ERR;
	
	return waddstr(win, message);
}

int wprintw(WINDOW* win, const char* fmt, ...) {
	va_list ap;
	int ret;

	va_start(ap, fmt);
	ret = vwprintw(win, fmt, ap);
	va_end(ap);
	return ret;
}

int mvwprintw(WINDOW* win, int y, int x, const char* fmt, ...) {
	va_list ap;
	int ret;

	va_start(ap, fmt);

	ret = wmove(win, y, x);
	if (ret != ERR)
		ret = vwprintw(win, fmt, ap);
		
	va_end(ap);
	return ret;
}

int mvprintw(int y, int x, const char* fmt, ...) {
	va_list ap;
	int ret;

	va_start(ap, fmt);

	ret = move(y, x);
	if (ret != ERR)
		ret = vwprintw(stdscr, fmt, ap);
		
	va_end(ap);
	return ret;
}


int printw(const char* fmt, ...) {
	va_list ap;
	int ret;

	va_start(ap, fmt);
	ret = vwprintw(stdscr, fmt, ap);
	va_end(ap);
	return ret;
}

