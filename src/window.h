/*
 * Copyright (c) 2012-2013, x0r
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */


#ifndef __MCRS_WINDOW
#define __MCRS_WINDOW

#include <stdint.h>

#define OUTOPT_CLEAROK	1
#define OUTOPT_IDLOK	2
#define OUTOPT_IDCOK	4
#define OUTOPT_IMMEDOK	8
#define OUTOPT_LEAVEOK	16
#define OUTOPT_NONL	32
#define OUTOPT_MASK	63

#define CHECK_VALID_WINDOW(win) \
	if((win) == NULL || (win)->data == NULL) return ERR;

#ifndef __MINICURSES_H
typedef struct WINDOW WINDOW;
#endif

uint8_t get_outopts(WINDOW* win);
int set_outopt(WINDOW* win, uint8_t outopts, int bf);
int cursor_getattr(WINDOW* win);
int cursor_getx(WINDOW* win);
int cursor_gety(WINDOW* win);

#endif
