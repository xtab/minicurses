/*
 * Copyright (c) 2012-2013, x0r
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */

#include "include/minicurses.h"
#include "color.h"

#include <stdlib.h>

static struct color_pair* __color_pairs;
static int __start_color_done = 0;


/* exported functions */
int has_colors() {
	return 1;
}

int can_change_color() {
	return 0;
}

int start_color() {
	int i;

	/* Don't initialize more than once */
	if (__start_color_done)
		return OK;

	__color_pairs = malloc(COLOR_PAIRS * sizeof(struct color_pair));
	if (__color_pairs == NULL)
		return ERR;
	
	for (i = 0; i < COLOR_PAIRS; i++) {
		if (__init_pair(i, COLOR_CYAN, COLOR_BLACK) == ERR)
			return ERR;
	}

	__start_color_done = 1;
	return OK;
}


int init_pair(short pair, short f, short b) {
	if (pair <= 0 || pair > COLOR_PAIRS)
		return ERR;
	if (! __start_color_done)
		return ERR;
	return __init_pair(pair, f, b);
}


int pair_content(short pair, short* f, short* b) {
	if (pair <= 0 || pair > COLOR_PAIRS)
		return ERR;
	if (! __start_color_done)
		return ERR;
	
	return __pair_content(pair, f, b);
}

/* private functions */
int __init_pair(short pair, short f, short b) {
	__color_pairs[pair].fg = f;
	__color_pairs[pair].bg = b;
	return OK;
}

int __pair_content(short pair, short* f, short* b) {
	if (pair == 0) {
		*f = COLOR_CYAN;
		*b = COLOR_BLACK;
	}
	else
	{
		if (f != NULL)
			*f = __color_pairs[pair].fg;
		if (b != NULL)
			*b = __color_pairs[pair].bg;
	}
	return OK;
}

