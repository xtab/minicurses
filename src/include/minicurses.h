/*
 * Copyright (c) 2012-2013, x0r
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */


#ifndef __MINICURSES_H
#define __MINICURSES_H


/* Some useful constants */
#define LINES		24
#define COLS		40
#define COLORS		8
#define COLOR_PAIRS	64

#define OK		0
#define ERR		-1

/* Zone and character attributes */

/* Color constants */
#define COLOR_BLACK 	0
#define COLOR_RED	1
#define COLOR_GREEN	2
#define COLOR_YELLOW	3
#define COLOR_BLUE	4
#define COLOR_MAGENTA	5
#define COLOR_CYAN	6
#define COLOR_WHITE	7

/* Character attributes */
#define __ACS_SHIFT	8
#define	__GRAPHIC_SHIFT	9
#define __PAIR_SHIFT	16
#define __SIZE_SHIFT	22
#define __ULINE_SHIFT	24
#define __INVIS_SHIFT	25
#define __REV_SHIFT	26
#define __BLINK_SHIFT	27


#define A_CHARTEXT	((1 << __ACS_SHIFT) - 1)

#define A_ALTCHARSET	(1 << __ACS_SHIFT)

#define A_NORMALSIZE	(0 << __SIZE_SHIFT)
#define A_DOUBLEHEIGHT	(1 << __SIZE_SHIFT)
#define A_DOUBLEWIDTH	(2 << __SIZE_SHIFT)
#define A_DOUBLESIZE	(3 << __SIZE_SHIFT)

#define A_BLINK		(1 << __BLINK_SHIFT)
#define A_REVERSE	(1 << __REV_SHIFT)
#define A_GRAPHIC	(1 << __GRAPHIC_SHIFT)

#define COLOR_PAIR(n)	((n) << __PAIR_SHIFT)

/* Character/zone attributes */
#define A_INVIS		(1 << __INVIS_SHIFT)
#define A_UNDERLINE	(1 << __ULINE_SHIFT)

#define ATTR_MASK	(((1 << 19) - 1) << __GRAPHIC_SHIFT)
#define COLOR_PAIR_MASK	COLOR_PAIR(COLOR_PAIRS - 1)
#define COLOR_PAIR_OF(mc) (((mc) & (COLOR_PAIR_MASK)) >> __PAIR_SHIFT)

#define A_NORMAL	0

/* includes we need */
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>



typedef uint32_t mchar;
typedef uint32_t chtype;

typedef struct screen SCREEN;
typedef struct vtx_pixmap PIXMAP;
typedef struct WINDOW WINDOW;

extern WINDOW* stdscr;

void initscr();
void endwin();
/* exported directly from screen.c */
SCREEN* newterm(char* type, FILE* outfd, FILE* infd); 
SCREEN* set_term(SCREEN* new);
void delscreen(SCREEN* scr);

/* exported directly from window.c */
WINDOW* newwin(int nlines, int ncols, int begin_y, int begin_x);
int delwin(WINDOW* win);

int wmove(WINDOW* win, int y, int x);
#define move(y, x) wmove(stdscr, (y), (x))

/* exported directly from window.c */
int waddch(WINDOW* win, const chtype c);
#define mvwaddch(win, y, x, c) \
		((wmove((win), (y), (x)) == ERR) ? ERR : waddch((win), (c)))
#define mvaddch(y, x, c) mvwaddch(stdscr, (y), (x), (c))
#define addch(c) waddch(stdscr, (c))

/* exported directly from window.c */
int waddstr(WINDOW* w, const char* s);
#define mvwaddstr(win, y, x, s) \
		((wmove((win), (y), (x)) == ERR) ? ERR : waddstr((win), (s)))
#define mvaddstr(y, x, s) mvwaddstr(stdscr, (y), (x), (s))
#define addstr(s) waddstr(stdscr, (s))

/* exported directly from window.c */
int wattron(WINDOW* win, uint32_t attr);
#define attron(attr) wattron(stdscr, (attr))
int wattroff(WINDOW* win, uint32_t attr);
#define attroff(attr) wattroff(stdscr, (attr))
int wattrset(WINDOW* win, uint32_t attr);
#define attrset(attr) wattrset(stdscr, (attr))

int clearok(WINDOW* win, int bf);

/* exported directly from window.c */
int werase();
#define erase() werase(stdscr)
#define wclear(win) (werase(win) == ERR ? ERR : clearok(win, 1))
#define clear() wclear(stdscr)

/* exported directly from screen.c */
int doupdate();
/* exported directly from window.c */
int wnoutrefresh(WINDOW* win);
#define wrefresh(win) (wnoutrefresh(win) == ERR ? ERR : doupdate())
#define refresh() wrefresh(stdscr)


/* exported directly from color.c */
int has_colors();
int can_change_color();
int start_color();
int init_pair(short pair, short f, short b);
int pair_content(short pair, short* f, short* b);

/* exported directly from image.c */
void start_graphics(char* progname);
int init_image(PIXMAP** pm, char* filename);
void destroy_image(PIXMAP* pm);
int waddimg(WINDOW* w, PIXMAP* pm);
#define mvwaddimg(win, y, x, pm) \
		((wmove((win), (y), (x)) == ERR) ? ERR : waddimg((win), (pm)))
#define mvaddimg(y, x, pm) mvwaddimg(stdscr, (y), (x), (pm))
#define addimg(pm) waddimg(stdscr, (pm))

/* exported from lib_printw.c */
int vwprintw(WINDOW* win, const char* fmt, va_list ap);
int wprintw(WINDOW* win, const char* fmt, ...);
int mvwprintw(WINDOW* win, int y, int x, const char* fmt, ...);
int mvprintw(int y, int x, const char* fmt, ...);
int printw(const char* fmt, ...);
#define vw_printw vwprintw


int beep();

int curs_set(int visibility);

#endif
