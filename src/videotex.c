/*
 * Copyright (c) 2012-2013, x0r
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */

#include <stdio.h>

#include "screen.h"
#include "color.h"
#include "videotex.h"


#define CHECK_VALID_FILEPTR(fp) if ((fp) == NULL) return



void minitel_cls(FILE* outfd) 
{
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, VT_CLS);
}

void minitel_cup(FILE* outfd, int y, int x) 
{
	CHECK_VALID_FILEPTR(outfd);
	if (x < 0 || x > COLS - 1) 
		x = 0;
	if (y < 0 || y > LINES - 1)
		y = 0;

	
	fprintf(outfd, "\x1f%c%c", (char)('A' + y), (char)('A' + x));

}

void minitel_char(FILE* outfd, unsigned char c)
{
	CHECK_VALID_FILEPTR(outfd);
	switch (c)
	{
		case 163: /* POUND SIGN */
		case 167: /* SECTION SIGN */
		case 176: /* DEGREE SIGN */
		case 177: /* PLUS-MINUS SIGN */
			fprintf(outfd, VT_SS2 "%c", c & 0x7f);
			break;

		case 188: /* LATIN CAPITAL LIGATURE OE */
			fprintf(outfd, VT_SS2 "j");
			break;

		case 189: /* LATIN SMALL LIGATURE OE */
			fprintf(outfd, VT_SS2 "z");
			break;

		case 223: /* LATIN SMALL LETTER SHARP S */
			fprintf(outfd, VT_SS2 "{");

		case 192: case 193: case 194: case 195: case 196: case 197:
			fprintf(outfd, "A");
			break;
		case 199:
			fprintf(outfd, "C");
			break;
		case 200: case 201: case 202: case 203:
			fprintf(outfd, "E");
			break;
		case 204: case 205: case 206: case 207:
			fprintf(outfd, "I");
			break;
		case 210: case 211: case 212: case 213: case 214: case 216:
			fprintf(outfd, "O");
			break;
		case 217: case 218: case 219: case 220:
			fprintf(outfd, "U");
			break;

		case 224: fprintf(outfd, VT_SS2 "Aa"); break;
		case 225: fprintf(outfd, VT_SS2 "Ba"); break;
		case 226: fprintf(outfd, VT_SS2 "Ca"); break;
		case 227: fprintf(outfd, "a"); break;
		case 228: fprintf(outfd, VT_SS2 "Ha"); break;
		case 229: fprintf(outfd, "a"); break;

		case 231: fprintf(outfd, VT_SS2 "Kc"); break;

		case 232: fprintf(outfd, VT_SS2 "Ae"); break;
		case 233: fprintf(outfd, VT_SS2 "Be"); break;
		case 234: fprintf(outfd, VT_SS2 "Ce"); break;
		case 235: fprintf(outfd, VT_SS2 "He"); break;

		case 236: fprintf(outfd, VT_SS2 "Ai"); break;
		case 237: fprintf(outfd, VT_SS2 "Bi"); break;
		case 238: fprintf(outfd, VT_SS2 "Ci"); break;
		case 239: fprintf(outfd, VT_SS2 "Hi"); break;

		case 242: fprintf(outfd, VT_SS2 "Ao"); break;
		case 243: fprintf(outfd, VT_SS2 "Bo"); break;
		case 244: fprintf(outfd, VT_SS2 "Co"); break;
		case 245: fprintf(outfd, VT_SS2 "o"); break;
		case 246: fprintf(outfd, VT_SS2 "Ho"); break;

		case 249: fprintf(outfd, VT_SS2 "Au"); break;
		case 250: fprintf(outfd, VT_SS2 "Bu"); break;
		case 251: fprintf(outfd, VT_SS2 "Cu"); break;
		case 252: fprintf(outfd, VT_SS2 "Hu"); break;

		/* TODO: small letters*/

		default:
			if (c < 127) 
				fprintf(outfd, "%c", c);
			else
				fprintf(outfd, "?");
	}
}

void minitel_rep(FILE* outfd, int rep) {
	int count;
	CHECK_VALID_FILEPTR(outfd);
	while (rep > 0) 
	{
		count = (rep > 63) ? 63 : rep;
		rep -= count;
		fprintf(outfd, VT_REP "%c", (char)('@' + count));
	}

}


void minitel_beep(FILE* outfd) {
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, VT_BEL);
}





void minitel_chattr(FILE* outfd, uint32_t new_flags, uint32_t old_flags)
{
	uint32_t diff;
	CHECK_VALID_FILEPTR(outfd);

	diff = (old_flags ^ new_flags) & ATTR_MASK;	

	if ((diff & COLOR_PAIR_MASK) && ! (new_flags & A_GRAPHIC)) {
		short fg, bg;
		if (COLOR_PAIR_OF(new_flags)) {
			if (pair_content(COLOR_PAIR_OF(new_flags), &fg, &bg) != ERR) {
				minitel_fgcolor(outfd, fg);
				minitel_bgcolor(outfd, bg);
			}
		}
		else {
			minitel_fgcolor(outfd, COLOR_CYAN);
			minitel_bgcolor(outfd, COLOR_BLACK);
		}
	}
	else if ((diff & COLOR_PAIR_MASK) && (new_flags & A_GRAPHIC)) {
		short fg, bg;
		int color_pair = COLOR_PAIR_OF(new_flags);
		fg = color_pair & 0x7;
		bg = (color_pair >> 3) & 0x7;
		minitel_fgcolor(outfd, fg);
		minitel_bgcolor(outfd, bg);
	}

	if (diff & A_DOUBLESIZE)
		minitel_size(outfd, (new_flags & A_DOUBLESIZE) >> __SIZE_SHIFT);

	if (diff & A_BLINK)
		minitel_blink(outfd, new_flags & A_BLINK);

	if (diff & A_REVERSE)
		minitel_reverse(outfd, new_flags & A_REVERSE);

	if (diff & A_GRAPHIC)
		minitel_graphic(outfd, new_flags & A_GRAPHIC);

	if (diff & A_INVIS)
		minitel_invisible(outfd, new_flags & A_INVIS);

	if (diff & A_UNDERLINE)
		minitel_underline(outfd, new_flags & A_UNDERLINE);

}




void minitel_flush(FILE* outfd)
{
	CHECK_VALID_FILEPTR(outfd);
	fflush(outfd);
}


void minitel_cursor(FILE* outfd, int visibility)
{
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, "%s", (visibility) ? VT_CON : VT_COFF);
}




void minitel_fgcolor(FILE* outfd, int color)
{
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, VT_ESC "%c", (char)('@' + color));
}

void minitel_bgcolor(FILE* outfd, int color)
{
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, VT_ESC "%c", (char)('P' + color));
}

void minitel_size(FILE* outfd, int size)
{
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, VT_ESC "%c", (char)('L' + size));
}

void minitel_blink(FILE* outfd, int blink)
{
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, VT_ESC "%c", (blink) ? 'H' : 'I');
}

void minitel_reverse(FILE* outfd, int rev)
{
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, VT_ESC "%c", (rev) ? ']' : '\\');
}

void minitel_graphic(FILE* outfd, int gr)
{
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, "%s", (gr) ? VT_SS1 : VT_SS0);
}

void minitel_invisible(FILE* outfd, int invis)
{
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, VT_ESC "%c", (invis) ? 'X': '_');
}

void minitel_underline(FILE* outfd, int ul)
{
	CHECK_VALID_FILEPTR(outfd);
	fprintf(outfd, VT_ESC "%c", (ul) ? 'Z' : 'Y');
}
